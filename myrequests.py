import requests

def request_to_yandex():
    header = {'X-Yandex-API-Key': 'c255a4df-d2a1-420d-bc4b-adfedb356ca0'}
    url = 'https://api.weather.yandex.ru/v1/forecast?lat=55.75396&lon=37.620393&extra=true'
    response = requests.get(url=url, headers=header)
    return response.json()