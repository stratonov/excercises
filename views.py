from django.db.models import Avg
from django.shortcuts import render
from rest_framework import viewsets, status, generics, mixins
from rest_framework.decorators import action

from .models import Weather
from .serializers import WeatherSerializer, GetWeatherSerializer, WeatherInfoSerializer
from .myrequests import request_to_yandex
from rest_framework.response import Response
from rest_framework.views import APIView

def get_data():
    content = request_to_yandex()
    serializer = WeatherSerializer(content)
    return Response(serializer.data)

#ручка для получения средней температуры по всем записям
class WeatherInfo(viewsets.ModelViewSet):
    queryset = Weather.objects.all()
    serializer_class = GetWeatherSerializer

    @action(methods=['GET'], detail=False, url_path='get-avg',url_name='get-avg')
    def get_avg(self, request):
        all_temps = Weather.objects.values_list('temp')
        sum = 0
        for temp in all_temps:
            sum += temp[0]
        return Response(data={'avg-temp':sum/len(all_temps)})

    def list(self, request, *args, **kwargs):

        serializer = GetWeatherSerializer(Weather.objects.last())
        return Response(serializer.data)


class WeatherViewSet(viewsets.ModelViewSet):
    queryset = Weather.objects.all()
    serializer_class = GetWeatherSerializer

    #добавление записи из YandexAPI
    def create(self, request):
        content = request_to_yandex()
        serializer = WeatherSerializer(content)
        weather = Weather.objects.create(now_dt=serializer.data['now_dt'],
                                         latitude=serializer.data['info']['lat'],
                                         longitude=serializer.data['info']['lon'],
                                         name=serializer.data['info']['tzinfo']['name'],
                                         def_pressure=serializer.data['fact']['pressure_mm'],
                                         temp=serializer.data['fact']['temp'],
                                         temperature_feels=serializer.data['fact']['feels_like'],
                                         wind_speed=serializer.data['fact']['wind_speed'],
                                         wind_direction=serializer.data['fact']['wind_dir']
                                         )
        return Response(status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):

        serializer = GetWeatherSerializer(Weather.objects.last())
        return Response(serializer.data)
