from rest_framework import serializers
from .models import Weather

class WeatherSerializer(serializers.Serializer):
    now_dt = serializers.DateTimeField()
    info = serializers.DictField()
    fact = serializers.DictField()

class GetWeatherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Weather
        fields = '__all__'

class WeatherInfoSerializer():
    temp__avg = serializers.DecimalField(max_digits=None, decimal_places=None)


    #temp = serializers.IntegerField()
    #class Meta:
     #   model = Weather
      #  fields = '__all__'
