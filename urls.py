from rest_framework.routers import DefaultRouter
from django.urls import path, include
from . import views

router = DefaultRouter()

router.register('info', views.WeatherInfo)
#router.register((r'info'), views.WeatherInfo.as_view())

urlpatterns = [
    path('', include(router.urls)),
]