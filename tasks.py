#ДЛЯ ЗАПУСКА ТАСОК НА ВИНДЕ НЕОБХОДИМ eventlet/gevent
#WORKER: celery -A weatherAPI worker --pool=gevent -l debug
#BEAT: celery -A weatherAPI beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
#WORKER и BEAT должны быть запущены параллельно!!!
from weatherAPI.celery import app
from .serializers import WeatherSerializer
from .myrequests import request_to_yandex
from .models import Weather
from celery import shared_task

app.conf.beat_schedule = {
    'post_data_from_yandex': {
        'task': 'weather.tasks.post_data_from_yandex',
        'schedule': 10.0,
    },
    'add': {
        'task': 'weather.tasks.add',
        'schedule': 5.0,
        'args': (2, 87)
    }
}

@shared_task
def add(x, y):
    return x + y

@app.task
def post_data_from_yandex():
    content = request_to_yandex()
    serializer = WeatherSerializer(content)
    weather = Weather.objects.create(now_dt=serializer.data['now_dt'],
                                         latitude=serializer.data['info']['lat'],
                                         longitude=serializer.data['info']['lon'],
                                         name=serializer.data['info']['tzinfo']['name'],
                                         def_pressure=serializer.data['fact']['pressure_mm'],
                                         temp=serializer.data['fact']['temp'],
                                         temperature_feels=serializer.data['fact']['feels_like'],
                                         wind_speed=serializer.data['fact']['wind_speed'],
                                         wind_direction=serializer.data['fact']['wind_dir'])
