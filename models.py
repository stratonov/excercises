from django.db import models
from django.db.models import Avg


class Weather(models.Model):
    now_dt = models.DateTimeField()
    latitude = models.DecimalField(decimal_places=5, max_digits=10, null=True)
    longitude = models.DecimalField(decimal_places=5, max_digits=10, null=True)
    name = models.CharField(max_length=100, default="ok", null=True)
    def_pressure = models.IntegerField(null=True)
    temp = models.IntegerField(default=1, null=True)
    temperature_feels = models.IntegerField(null=True)
    wind_speed = models.IntegerField(null=True)
    wind_direction = models.CharField(max_length=2, null=True)

    def __str__(self):
        return str(self.now_dt)

    # @property
    # def avg_temp(self):
    #     return self.objects.all().aggregate(Avg('temp'))